class Graphics
{
    constructor(c)
    {
        // Class variables
        this.gl;
        this.program;
        this.canvas = c;
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.mouseX = 0;
        this.mouseY = 0;
        
        // Seting up canvas width and height
        this.canvas.width = this.width;
        this.canvas.height = this.height;

        this.blubsArray;

        // Setup WebGL
        this.gl = this.canvas.getContext('webgl');

        // If not normal webgl, test for an expierimental version
        if(!this.gl)
            this.gl = canvas.getContext('experimental-webgl');
        
        // All else fails just alert the user that the game is not suported
        if(!this.gl)
            alert("Your browser does not support WebGL.");

        this.shaderSet = [
            {
                type: this.gl.VERTEX_SHADER,
                id: "vertex-shader"
            },
            {
                type: this.gl.FRAGMENT_SHADER,
                id: "fragment-shader"
            }
            ];

       this.init();
    }

    init()
    {
        // Set the clear color, and clear both gl buffers before starting to render anything
        this.gl.clearColor(0.3, 0.3, 0.3, 1.0);
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);

        let vertexShaderText = document.getElementById('vertex-shader').firstChild.nodeValue;
        let fragmentShaderText = document.getElementById('fragment-shader').firstChild.nodeValue;

        var vertexShader = this.gl.createShader(this.gl.VERTEX_SHADER);
        var fragmentShader = this.gl.createShader(this.gl.FRAGMENT_SHADER);

        this.gl.shaderSource(vertexShader, vertexShaderText);
        this.gl.shaderSource(fragmentShader, fragmentShaderText);

        this.gl.compileShader(vertexShader);
        if(!this.gl.getShaderParameter(vertexShader, this.gl.COMPILE_STATUS))
        {
            console.error("ERROR compiling vertex shader: ", this.gl.getShaderInfoLog(vertexShader));
            return;
        }

        this.gl.compileShader(fragmentShader);
        if(!this.gl.getShaderParameter(fragmentShader, this.gl.COMPILE_STATUS))
        {
            console.error("ERROR compiling fragment shader: ", this.gl.getShaderInfoLog(fragmentShader));
            return;
        }
        
        // Combine the shaders into a "program"
        this.program = this.gl.createProgram();
        this.gl.attachShader(this.program, vertexShader);
        this.gl.attachShader(this.program, fragmentShader);

        // Link program together
        this.gl.linkProgram(this.program);
        if(!this.gl.getProgramParameter(this.program, this.gl.LINK_STATUS))
        {
            console.error("ERROR linking shader program: ", this.gl.getProgramInfoLog(this.program));
            return;
        }
    }

    setBlubsArray(blubs)
    {
        this.blubsArray = blubs;
    }

    setMousePosition(x, y)
    {
        this.mouseX = x;
        this.mouseY = y;
    }

    draw()
    {
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);

        var buffer;
	
        buffer = this.gl.createBuffer();
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, buffer);

        if(this.blubsArray)
        {
            var objectsToRender = new Float32Array(this.blubsArray.length * 2);
            var index = 0;

            for(var i = 0; i < this.blubsArray.length; i++)
            {
                //const x = parseFloat(this.blubsArray[i].x / this.width  *  2.0 - 1.0).toPrecision(2);
                //const y = parseFloat(this.blubsArray[i].y / this.height * -2.0 + 1.0).toPrecision(2);
                const x = parseFloat(this.blubsArray[i].x).toPrecision(2);
                const y = parseFloat(this.blubsArray[i].y).toPrecision(2);
                objectsToRender[index] = x;
                objectsToRender[index + 1] = y;
                index = index + 2;
            }
        }

        this.gl.useProgram(this.program);

        var blob_data = this.gl.getUniformLocation(this.program, "blob_location");
        this.gl.uniform2fv(blob_data, objectsToRender);

        var mouse_location = this.gl.getUniformLocation(this.program, "mouse");
        this.gl.uniform2fv(mouse_location, [this.mouseX, this.mouseY]);

        var resolution = this.gl.getUniformLocation(this.program, 'resolution');
        this.gl.uniform2fv(resolution, [this.width.toFixed(1), this.height.toFixed(1)]);

        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(
            [
                -1.0, -1.0, 
                1.0, -1.0, 
                -1.0,  1.0, 
                -1.0,  1.0, 
                1.0, -1.0, 
                1.0,  1.0,
            ]
            ), this.gl.STATIC_DRAW
        );
        
        var positionLocation = this.gl.getAttribLocation(this.program, "a_position");
        this.gl.enableVertexAttribArray(positionLocation);
        this.gl.vertexAttribPointer(positionLocation, 2, this.gl.FLOAT, false, 0, 0);

        // void gl.vertexAttribPointer(index, size, type, normalized, stride, offset);

        this.gl.drawArrays(this.gl.TRIANGLES, 0, 6);
    }

    clear()
    {
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
    }

    resize(w, h)
    {
        this.width = window.innerWidth;
        this.height = window.innerHeight;

        this.canvas.width = this.width;
        this.canvas.height = this.height;

        this.gl.viewport(0, 0, this.width, this.height);
    }
}

