var socket = new WebSocket('ws://0.0.0.0:8080');
var blubs;
var graphics;

var running = false;
var fps_counter;
var fps;
var previous;

socket.onopen = function() 
{
    socket.send('hello');
};

socket.onmessage = function(s) 
{
    var obj = JSON.parse(s.data);
    blubs = obj.blubs;

    graphics = new Graphics(canvas);
    graphics.setBlubsArray(blubs);

    running = true;
};

document.onmousemove = function(event)
{
    if(running)
    {
        graphics.setMousePosition(event.x.toFixed(1), event.y.toFixed(1));
    }
};

function loop()
{
    if(!previous)
    {
        previous = performance.now();
    }

    if(running)
    {
        var delta = (performance.now() - previous) / 1000;
        previous = performance.now();
        fps = 1 / delta;

        graphics.draw();

        fps_counter.innerHTML = "" + Math.ceil(fps);
    }

    requestAnimationFrame(loop);
};

window.onload = function()
{
    var canvas = document.getElementById("canvas");
    fps_counter = document.getElementById("fps-counter-text");
    requestAnimationFrame(loop);
};

/*
function loop()
{
    requestAnimationFrame(loop);
    if(running)
    {
        graphics.draw();
    }
};
*/