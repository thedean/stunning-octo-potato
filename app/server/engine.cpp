#include "engine.hpp"

engine::engine()
{
    m_running = false;
}

void engine::run()
{
    duration<double> past_time = duration<double>::zero();
    duration<double> total_passed_time = duration<double>::zero();
    double delta_time = 0.0;
    int ticks = 0;

    while(m_running)
    {
        m_start = high_resolution_clock::now();

        // You should put some kind of bounds on delta_time
        update(delta_time);
        ticks++;

        m_stop = high_resolution_clock::now();

        // Calculate the time the update took
        past_time = duration_cast<duration<double>> (m_stop - m_start);
        // Add the time to the total time
        total_passed_time += past_time;
        // Convert the passed time to delta time
        delta_time = past_time.count();

        // If the total passed time is greater than a second print the update count
        if(total_passed_time.count() > 1.0)
        {
            //std::cout << ticks;
            //std::cout << " ticks per second" << std::endl;
            logger::log(LOG_INFO, std::to_string(ticks) + " avg ticks per second");

            // reset the variables
            total_passed_time = duration<double>::zero();
            ticks = 0;
        }
    }
}

void engine::update(double dt)
{
    // Do a bunch of nothing so the game doesn't have a cow
    for(int i = 0; i < 1000; i++)
    {
        int x = 0;
    }
}

void engine::start()
{
    m_running = true;
    run();
}

void engine::stop()
{
    m_running = false;
}