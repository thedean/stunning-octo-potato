#include <boost/lambda/lambda.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ip/udp.hpp>

#include <cstdlib>
#include <functional>
#include <iostream>
#include <string>
#include <thread>

#include "listener.hpp"
#include "blub.hpp"
#include "engine.hpp"
#include "qtree.hpp"

namespace beast = boost::beast;
namespace http = beast::http;
namespace websocket = beast::websocket;
namespace net = boost::asio;
using tcp = boost::asio::ip::tcp;
using udp = boost::asio::ip::udp;

void do_session(tcp::socket&);

int main(int argc, char* argv[])
{
    try
    {
        // Check command line arguments.
        if (argc != 4)
        {
            std::cerr <<
                "Usage: name <address> <port> <threads>\n" <<
                "Example:\n" <<
                "     0.0.0.0 8080 1\n";
            return EXIT_FAILURE;
        }

        //Create variables to store the address and port number to use
        auto const address = net::ip::make_address(argv[1]);
        auto const port = static_cast<unsigned short>(std::atoi(argv[2]));
        auto const threads = std::max<int>(1, std::atoi(argv[3]));

        // The io_context is required for all I/O
        net::io_context ioc{threads};

        std::make_shared<listener>(ioc, tcp::endpoint{address, port})->run();

        // Run the I/O service on the requested number of threads
        std::vector<std::thread> v;
        v.reserve(threads - 1);
        for(auto i = threads - 1; i > 0; --i)
            v.emplace_back(
            [&ioc]
            {
                ioc.run();
            });

        // Do other things here!!!
        //ioc.run();

        // Create and run the engine, might create the engine somewhere else...
        engine e;
        e.start();

        // Join Threads (Boost Documentation)
    }
    catch (const std::exception& e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
};

// For Logging, you should do something like this... but in C++
// Also if you are feeling lazy: https://github.com/amrayn/easyloggingpp

/*
module.exports = function Log(logName, info, reason, newLine) {
    if (reason === "finish") {
        console.log('\x1b[32m%s\x1b[0m', "[COMPLETE] @" + logName + ": " + info + ".");
    } else if (reason === "error") {
        console.log('\x1b[31m%s\x1b[0m', "[ERROR] @" + logName + ": " + info + ".");
    } else if (reason === "info") {
        console.log('\x1b[36m%s\x1b[0m', "[INFO] @" + logName + ": " + info + ".");
    } else if (reason === "warn") {
        console.log('\x1b[33m%s\x1b[0m', "[WARNING] @" + logName + ": " + info + ".");
    } else {
        console.log("@" + logName + ": " + info + ".");
    }

    if (newLine === true) {
        console.log();
    }
}
*/

// You could make the output look something like this: 

/*
0: 10/7/2017 23:9:22	INFO:    mainThread:	The file logger was created successfully.
1: 10/7/2017 23:9:22	INFO:    mainThread:	The high-precision timer was created successfully.
2: 10/7/2017 23:9:22	INFO:    mainThread:	The client resolution was read from the Lua configuration file: 800 x 600.
3: 10/7/2017 23:9:22	WARNING: mainThread:	The window was resized. The game graphics must be updated!
4: 10/7/2017 23:9:22	INFO:    mainThread:	The main window was successfully created.
5: 10/7/2017 23:9:22	INFO:    mainThread:	The DirectX application initialization was successful.
*/