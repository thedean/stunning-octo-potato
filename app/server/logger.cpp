#include "logger.hpp"

// MAKE THIS BETTER!!!!

void logger::log(int s, std::string str)
{

    auto time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::string time_string = ctime(&time);

    if(s == -1)
    {
        time_string.erase(std::remove(time_string.begin(), time_string.end(), '\n'), time_string.end());
        printf("%s [ DEBUG ] %s\n", time_string.c_str(), str.c_str());
    }
    else if (s == 0)
    {
        time_string.erase(std::remove(time_string.begin(), time_string.end(), '\n'), time_string.end());
        printf("%s [ INFO  ] %s\n", time_string.c_str(), str.c_str());
    }
    else if(s == 1)
    {
        time_string.erase(std::remove(time_string.begin(), time_string.end(), '\n'), time_string.end());
        printf("%s [WARNING] %s\n", time_string.c_str(), str.c_str());
    }
    else if(s == 2)
    {
        time_string.erase(std::remove(time_string.begin(), time_string.end(), '\n'), time_string.end());
        printf("%s [ ERROR ] %s\n", time_string.c_str(), str.c_str());
    }
  
}