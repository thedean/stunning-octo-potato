#ifndef POINT_QTREE_H
#define POINT_QTREE_H

#include <iostream> 
#include <cmath> 
using namespace std; 

struct Point
{
    int x;
    int y;

    Point(int, int);

    Point();
};

#endif

#ifndef NODE_QTREE_H
#define NODE_QTREE_H

struct Node
{
    Point pos;
    int data; // Can become some other kind of data, i.e a blub...

    Node(Point, int);

    Node();
};

#endif

#ifndef QTREE_H
#define QTREE_H

class qtree
{
private:
    Point m_top_left_point;
    Point m_bottom_right_point;

    Node *m_node;

    qtree *m_top_left_tree;
    qtree *m_top_right_tree;
    qtree *m_bottom_left_tree;
    qtree *m_bottom_right_tree;

public:
    qtree();

    qtree(Point, Point);

    void insert(Node*);

    Node* search(Point);

    bool inBoundary(Point);
};

#endif