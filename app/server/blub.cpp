#include "blub.hpp"

// Constructor
blub::blub()
{
    // Random ID
    //milliseconds ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
    //m_id = abs(ms.count());
    m_id = 0;

    // Parent ID
    m_parent_id = 0;

    // Valid
    m_valid = false;

    // Position and size
    m_x = 300;
    m_y = 300;
    m_r = 1.0;

    // Color
    m_rgba[0] = 0.0;
    m_rgba[1] = 0.0;
    m_rgba[2] = 0.0;
    m_rgba[3] = 1.0;
}

// Valid
bool blub::isValid()
{
    return m_valid;
}

void blub::setValid(bool v)
{
    m_valid = v;
}

// Getters
int blub::getID()
{
    return m_id;
}

int blub::getParentID()
{
    return m_parent_id;
}

int blub::getX()
{
    return m_x;
}

int blub::getY()
{
    return m_y;
}

float blub::getRadius()
{
    return m_r;
}

float* blub::getColor()
{
    return m_rgba;
}

float blub::getRed()
{
    return m_rgba[0];
}

float blub::getGreen()
{
    return m_rgba[1];
}

float blub::getBlue()
{
    return m_rgba[2];
}

float blub::getAlpha()
{
    return m_rgba[3];
}

// Setters
void blub::setID(int id)
{
    m_id = id;
}

void blub::setParentID(int id)
{
    m_parent_id = id;
}

void blub::setX(int x)
{
    m_x = x;
}

void blub::setY(int y)
{
    m_y = y;
}

void blub::setRadius(float r)
{
    m_r = r;
}

void blub::setColor(float* c)
{
    m_rgba[0] = c[0];
    m_rgba[1] = c[1];
    m_rgba[2] = c[2];
    m_rgba[3] = c[3];
}

void blub::setRed(float r)
{
    m_rgba[0] = r;
}

void blub::setGreen(float g)
{
    m_rgba[1] = g;
}

void blub::setBlue(float b)
{
    m_rgba[2] = b;
}

void blub::setAlpha(float a)
{
    m_rgba[3] = a;
}

// JSON
std::string blub::getJSONString(std::string args[], int size)
{
    std::string returnString = "{";

    for(int i = 0; i < size; i++)
    {
        if(args[i] == "id")
        {
            returnString += "\"id\": \"" + std::to_string(m_id) + "\"";
        } 
        else if (args[i] == "x")
        {
            returnString += "\"x\": \"" + std::to_string(m_x) + "\"";
        } 
        else if (args[i] == "y")
        {
            returnString += "\"y\": \"" + std::to_string(m_y) + "\"";
        }
        else if (args[i] == "parent_id")
        {
            returnString += "\"parent_id\": \"" + std::to_string(m_parent_id) + "\"";
        }
        else if (args[i] == "r")
        {
            returnString += "\"r\": \"" + std::to_string(m_r) + "\"";
        }
        else if (args[i] == "rgba")
        {
            returnString += "\"rgba\": \"rgba(" + std::to_string(m_rgba[0]) + 
                            ", " + std::to_string(m_rgba[1]) + 
                            ", " + std::to_string(m_rgba[2]) + 
                            ", " + std::to_string(m_rgba[3]) + ")\"";
        }
        
        if(i < size - 1)
        {
            returnString += ", ";
        }
    }

    returnString += "}";

    return returnString;
}