#ifndef ENGINE_H
#define ENGINE_H

#include <chrono>
#include <iostream>

#include "logger.hpp"

using namespace std::chrono;

class engine
{

    
private:

    // Boolean if the game is running or not
    bool m_running;

    // Time variables
    high_resolution_clock::time_point m_start;
    high_resolution_clock::time_point m_stop;

    // Main run function
    void run();

    // Update
    void update(double);

public:
    // Constructor
    engine();

    // Start
    void start();

    // Stop
    void stop();
};

#endif