#ifndef BLUB_H
#define BLUB_H

#include <string>
#include <chrono>
#include <cstdarg>

using namespace std::chrono;

class blub
{
private:

    int m_id;           // ID of the blub itself, can be numbered sequencially
    int m_parent_id;    // ID of the parent blub if it has one
    bool m_valid;        // If the blub is currently valid or not
    int m_x;            // X position of the blub
    int m_y;            // Y position of the blub
    float m_r;          // Radius of the blub
    float m_rgba[4];    // Color of the blub, r, g, b, a

public: 
    // Constructor
    blub();

    // Valid bit functions
    bool isValid();
    void setValid(bool);

    // Getters
    int getID();
    int getParentID();
    int getX();
    int getY();
    float getRadius();
    float* getColor();
    float getRed();
    float getGreen();
    float getBlue();
    float getAlpha();

    // Setters
    void setID(int);
    void setParentID(int);
    void setX(int);
    void setY(int);
    void setRadius(float);
    void setColor(float*);
    void setRed(float);
    void setGreen(float);
    void setBlue(float);
    void setAlpha(float);

    // JSON
    std::string getJSONString(std::string[], int size);
};

#endif