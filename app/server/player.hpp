#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include <chrono>

using namespace std::chrono;

class player
{

private: 
    int m_id;
    int m_x;
    int m_y;

    //Create a blub object to hold references to the blubs that the player has

    //Create an array of all the blub objects that are producers that the player has

    std::string m_display_name;

public:
    player();

    //Getters
    int getID();
    int getX();
    int getY();

    //Setters
    void setX(int);
    void setY(int);
};

#endif