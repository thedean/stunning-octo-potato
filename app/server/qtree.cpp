#include "qtree.hpp"

Point::Point(int _x, int _y)
{
    x = _x;
    y = _y;
}

Point::Point()
{
    x = 0;
    y = 0;
}

Node::Node(Point _pos, int _data)
{
    pos = _pos;
    data = _data;
}

Node::Node()
{
    data = 0;
}

qtree::qtree()
{
    m_top_left_point = Point(0, 0); 
    m_bottom_right_point = Point(0, 0); 

    m_node = NULL;

    m_top_left_tree = NULL;
    m_top_right_tree = NULL;
    m_bottom_left_tree  = NULL;
    m_bottom_right_tree = NULL;
}

qtree::qtree(Point _topLeft, Point _topRight)
{
    m_top_left_point = _topLeft; 
    m_bottom_right_point = _topRight; 

    m_node = NULL;

    m_top_left_tree = NULL; 
    m_top_right_tree = NULL; 
    m_bottom_left_tree  = NULL; 
    m_bottom_right_tree = NULL; 
}

void qtree::insert(Node *node)
{
    
    // If the node is NULL return
    if(node == NULL)
    {
        return;
    }

    
    // If the node is not in the current quad return
    if(!inBoundary(node->pos))
    {
        return;
    }
    
    // If the quad is too small, return because we do not want to subdivide
    if(abs(m_top_left_point.x - m_bottom_right_point.x) <= 1 && abs(m_top_left_point.y - m_bottom_right_point.y) <= 1)
    {
        if(m_node == NULL)
        {
            m_node = node;
        }

        return;
    }    
    
    // If it made is this far possibly subdivide
    if ((m_top_left_point.x + m_bottom_right_point.x) / 2 >= node->pos.x)
    {
        // Indicates it is in the top left tree
        if( (m_top_left_point.y + m_bottom_right_point.y) / 2 >= node->pos.y)
        {
            if(m_top_left_tree == NULL)
            {
                m_top_left_tree = new qtree(Point(m_top_left_point.x, m_top_left_point.y), Point((m_top_left_point.x + m_bottom_right_point.x) / 2, (m_top_left_point.y + m_bottom_right_point.y) / 2));
                m_top_left_tree->insert(node);
            }
        }
        
        // Indicates it is in the bottom left tree
        else
        {
            if (m_bottom_left_tree == NULL) 
            {
                m_bottom_left_tree = new qtree(Point(m_top_left_point.x, (m_top_left_point.y + m_bottom_right_point.y) / 2), Point((m_top_left_point.x + m_bottom_right_point.x) / 2, m_bottom_right_point.y)); 
                m_bottom_left_tree->insert(node); 
            }
        }
    }
    else
    {
        // Indicates it is in the top right tree
        if ((m_top_left_point.y + m_bottom_right_point.y) / 2 >= node->pos.y) 
        { 
            if (m_top_right_tree == NULL) 
            {
                m_top_right_tree = new qtree(Point((m_top_left_point.x + m_bottom_right_point.x) / 2, m_top_left_point.y), Point(m_bottom_right_point.x, (m_top_left_point.y + m_bottom_right_point.y) / 2)); 
                m_top_right_tree->insert(node);
            } 
        } 
  
        // Indicates it is in the bottom right tree
        else
        { 
            if (m_bottom_right_tree == NULL) 
            {
                m_bottom_right_tree = new qtree(Point((m_top_left_point.x + m_bottom_right_point.x) / 2, (m_top_left_point.y + m_bottom_right_point.y) / 2), Point(m_bottom_right_point.x, m_bottom_right_point.y)); 
                m_bottom_right_tree->insert(node); 
            }
        } 
    }
}

Node* qtree::search(Point p)
{
    // Check if the current quad contains the point
    if(!inBoundary(p))
    {
        return NULL;
    }

    // If we are a quad of unit length then we cannot subdivide any further just return the node
    if(m_node != NULL)
    {
        return m_node;
    }

    if((m_top_left_point.x + m_bottom_right_point.x) / 2 >= p.x)
    {
        // Indicates top left tree 
        if ((m_top_left_point.y + m_bottom_right_point.y) / 2 >= p.y) 
        { 
            if (m_top_left_tree == NULL) 
            {
                return NULL;
            }
            return m_top_left_tree->search(p); 
        } 
  
        // Indicates bottom left tree 
        else
        { 
            if (m_bottom_left_tree == NULL) 
            {
                return NULL;
            } 
            return m_bottom_left_tree->search(p); 
        } 
    }
    else
    {
        // Indicates top right tree 
        if ((m_top_left_point.y + m_bottom_right_point.y) / 2 >= p.y) 
        { 
            if (m_top_right_tree == NULL) 
                return NULL; 
            return m_top_right_tree->search(p); 
        } 
  
        // Indicates bottom right tree 
        else
        { 
            if (m_bottom_right_tree == NULL) 
                return NULL; 
            return m_bottom_right_tree->search(p); 
        } 
    }
}

bool qtree::inBoundary(Point p)
{
    return (p.x >= m_top_left_point.x && p.x <= m_bottom_right_point.x && p.y >= m_top_left_point.y && p.y <= m_bottom_right_point.y);
}