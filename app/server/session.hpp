#ifndef SESSION_H
#define SESSION_H

#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/ip/udp.hpp>

#include <algorithm>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#include "player.hpp"
#include "blub.hpp"

namespace beast = boost::beast;         
namespace http = beast::http;           
namespace websocket = beast::websocket; 
namespace net = boost::asio;            
using tcp = boost::asio::ip::tcp;     
using udp = boost::asio::ip::udp; 

class session : public std::enable_shared_from_this<session>
{
    websocket::stream<beast::tcp_stream> ws_;
    beast::flat_buffer buffer_;
    player pl;

public:
    explicit session(tcp::socket&& socket) : ws_(std::move(socket)){}

    void fail(beast::error_code, char const*);
    void run();
    void on_run();
    void on_accept(beast::error_code);
    void do_read();
    void on_read(beast::error_code, std::size_t);
    void on_write(beast::error_code, std::size_t);
};

#endif