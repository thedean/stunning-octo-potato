#include "player.hpp"

player::player()
{
    milliseconds ms = duration_cast<milliseconds>(system_clock::now().time_since_epoch());
    m_id = ms.count();
    m_x = 0;
    m_y = 0;
}

int player::getID()
{
    return m_id;
}

int player::getX()
{
    return m_x;
}

int player::getY()
{
    return m_y;
}

void player::setX(int x)
{
    m_x = x;
}

void player::setY(int y)
{
    m_y = y;
}