#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <string>
#include <stdio.h> 
#include <stdlib.h> 
#include <time.h> 
#include <chrono> 
#include <algorithm>

#ifndef LOG_ERROR
#define LOG_ERROR 2
#endif

#ifndef LOG_WARNING
#define LOG_WARNING 1
#endif

#ifndef LOG_INFO
#define LOG_INFO 0
#endif

#ifndef LOG_DEBUG
#define LOG_DEBUG 1
#endif


class logger
{
public:
    static void log(int, std::string);
};

#endif