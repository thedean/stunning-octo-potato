#!/bin/bash

# Settable parameters
BUILDDIR="./build/debug"
BINDIR="./bin"
FILEDIR="./app/server"

echo "Building stunning-octo-potato"
echo ""

mkdir -p "$BUILDDIR"
mkdir -p "$BINDIR"

compile()
{
    for CURRENTFILE in main listener session engine player blub qtree logger
    do
        echo "$CURRENTFILE.cpp"
        g++ -I lib/boost_1_72_0 -c "$FILEDIR/$CURRENTFILE.cpp" -o "$BUILDDIR/$CURRENTFILE.o"
    done
}

link()
{
    g++ -pthread -lpthread \
    \
    "$BUILDDIR/main.o"     \
    "$BUILDDIR/listener.o" \
    "$BUILDDIR/session.o"  \
    "$BUILDDIR/engine.o"   \
    "$BUILDDIR/player.o"   \
    "$BUILDDIR/blub.o"     \
    "$BUILDDIR/qtree.o"    \
    "$BUILDDIR/logger.o"    \
    \
    -o "$BINDIR/stunning-octo-potato-debug"
}

echo "Compiling..."
compile
echo ""

echo "Linking..."
link
echo ""

echo "Output located in $BINDIR/"

echo "Build Finished"


