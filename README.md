# stunning-octo-potato

### Version: 0.0.1
## The Blub Project

This Project is an attempt at creating something fun and enjoyable, while also using the skills I have aquired over the course of a year in web development. 

This game will be a blub based game where you control a "colony" of phages that you attack other cells with to take them over in order to progress and get bigger and more powerful. 

This is the quest to become the ultimate life form, in terms of blubs of course.  

In case you were wondering blubs are just blobs, but I thought blobs were boring so they are called blubs, you're welcome:)

## Dependencies

* Boost Libraries 1.72.0 - [Boost_libraries](https://www.boost.org/users/history/)

The dependencies need to be located in the ```/lib``` folder in order to be located by the build script automatically. For example the boost libraries are located in: ```/lib/boost_1_72_0```

The version of the library that is mentioned here is the version that is built and tested with, any other version of the boost library may not work as intended. 

## How to build the server

Make sure all dependencies are located in the correct locations. The server has a build script located in the main repository folder called ```build_server.sh```. This script has no parameters and can be run by typing the connamd:

```
./build_server.sh
```

This should create a build folder with a debug directory, inside this should be the executable file. If that does not work and it throws an error, the build script might need to be classified as an executable on your system, this can be done with the following command: 

```
chmod +x ./build_server.sh
```

## How to run the server

The servers executable file can be located in the build folder, either in debug or release (release currently does not exist). 

The server executable takes in 3 parameters, the ip address it should run on, the port number, and the number of threads it should use. For example to call the executable type the following: 

```
./stunning-octo-potato-debug 0.0.0.0 8080 5
```

This would create the server with an address of 0.0.0.0, on port 8080, with 5 threads to use for connections. 

## How to build the client

Because the client currently does not use Nginx or have any kind of testing at all, it does not need to be built.

## How to run the client

The client currently does not have any html serving system therefore it has to be tested the cheesy way. Just double click on the ```index.html``` file and load the "client" in any modern web browser. The minor amount of testing that has been done has been completed using both firefox and Chrome web browsers, currently there are no promises it will work anywhere else.

Note: If you try to connect to the server without the client it should return the error: 

```
The WebSocket handshake Connection field is missing the upgrade token
```

This is because the server is only operating with socket protocol and not serving the HTML, Nginx will be the server front end that is in charge of load balancing, security, and page serving. If the desire is to connect anyway, a terminal can be used to "fake" the socket upgrade request and connect to the server for fun. 

## Basic Server Structure

```
            +-----------------------+
            | Client Machine 1...N  |
            |  +-----------------+  |
            |  |HTTPS Client     +---------------------+
            |  +-----------------+  |                  |
            |  +-----------------+  |                  |
            |  |Socket Client    |  |                  |
            |  +--------+--------+  |                  |
            |           |           |                  |
            +-----------------------+                  |
                        |                              |
                        v                              v
+-----------------------+----------------------------------------+
|                                             +----------------+ |
|            NGINX Proxy and Server           | HTTPS Server   | |
|                                             +----------------+ |
+----------------------+-----------------------------------------+
                       |
                       v
            +----------+------------+
            | Physical server 1...N |
            | +-------------------+ |
            | | Game Server       | |
            | | +---------------+ | |
            | | |Socket Connect | | |
            | | +---------------+ | |
            | | +---------------+ | |
            | | |Socket Update  | | |
            | | +---------------+ | |
            | +-------------------+ |
            +-----------------------+

```

## Questions?

If you got this far thats amazing, if there are any questions about anything please let me know through gitlab. If there are any requests there is currently not an official way of handling that but anything and everything suggested is appreciated. 

Thank you,
The Dean








