# How versions will work in this project

Version numbers will be separated with a decimal point (.) and contain the following information in order:

MAJOR.MINOR.INC

MAJOR - The major verison number, to be incremented during major feature improvements, 2 digits

MINOR - The minor version number, to be incremented for official fixes/updates, 2 digits

INC - The incrementation value, to be incremented every commit in master, 4 digits

It is assumed that the Major and Minor version dictates compatibility between a server and client. They should both have the exact MAJOR and MINOR version. 

### Examples:

```
Version: 0.0.1234
Version: 1.0.0044
Version: 0.1.1111
```